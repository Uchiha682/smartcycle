﻿using UnityEngine;
using System.Collections;
using System.Net;
using uPLibrary.Networking.M2Mqtt;
using uPLibrary.Networking.M2Mqtt.Messages;
using uPLibrary.Networking.M2Mqtt.Utility;
using uPLibrary.Networking.M2Mqtt.Exceptions;

using System;

public class mqttTest : MonoBehaviour {
	private MqttClient client;
	// Use this for initialization
	void Start () {
		// create client instance 
		client = new MqttClient("m16.cloudmqtt.com", 11620 , false , null ); 

		// register to message received 
		client.MqttMsgPublishReceived += client_MqttMsgPublishReceived; 

		string clientId = Guid.NewGuid().ToString(); 
		client.Connect(clientId, "avfsaghw", "2R2YRMR0Qdbv"); 

		// subscribe to the topic "/home/temperature" with QoS 2 
		client.Subscribe(new string[] { "2R2YRMR0Qdbv/esp/test" }, new byte[] { MqttMsgBase.QOS_LEVEL_AT_MOST_ONCE }); 

	}
	void client_MqttMsgPublishReceived(object sender, MqttMsgPublishEventArgs e) 
	{ 

		Debug.Log("Received from Milkcocoa: " + System.Text.Encoding.UTF8.GetString(e.Message)  );
	} 

	void OnGUI(){
		if ( GUI.Button (new Rect (20,40,100,30), "Hello")) {
			Debug.Log("sending...");
			client.Publish("esp/test", System.Text.Encoding.UTF8.GetBytes("ligado"), MqttMsgBase.QOS_LEVEL_EXACTLY_ONCE, false);
			Debug.Log("sent to Milkcocoa");
		}
	}
	// Update is called once per frame
	void Update () {

	}
}